import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue2';
import Components from 'unplugin-vue-components/vite'
import { VuetifyResolver } from 'unplugin-vue-components/resolvers'

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.css', 'resources/js/app.js'],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
        Components({
            resolvers: [VuetifyResolver()],
        }),
    ],
    resolve: {
        alias: {
            '@layouts': '/resources/js/layouts',
            '@pages': '/resources/js/pages',
            '@services': '/resources/js/services',
            '@components': '/resources/js/components',
            '@': '/resources/js',
            'vue': 'vue/dist/vue.js'
        },
    },
});
