import './bootstrap';
import '../css/app.css'; 

import Vue from 'vue'
import vuetify from "@/plugins/vuetify";
import router from './router';


new Vue({
    vuetify,
    router,
    template: `<router-view/>`
}).$mount("#app");