import axios from "axios";

export default class ProjectService {

    static getProjects(params) {
        return axios.get( `/api/project`,{ params: params });
    } 

    static postNewProject( project ){
        return axios.post( `/api/project`,  project );
    }

    static updateProject( project ){
        return axios.put( `/api/project/${project.id}`,  project );
    }

    static deleteProject( idProject ){
        return axios.delete( `/api/project/${idProject}`);
    }
    
    static pingRat(url) {
        return axios.get('/api/pingrat', {params: {project_url: url}});
    } 
}