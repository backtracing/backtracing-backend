import axios from "axios";

export default class EntryService {
    static getEntriesByIdLog(idLog){
        return axios.get( `/api/entry/${idLog}`);
    }

    static getNewEntries(idLog, idEntries){
        return axios.get( `/api/entry/${idLog}/new/${idEntries}`);
    }
}