import axios from "axios";

export default class LogService {
    static getProjectsWithLogs(params) {
        return axios.get( `/api/logs`,{ params: params });
    } 

    static getNewLogs(params) {
        return axios.get( `/api/logs/new`,{ params: params });
    } 
}