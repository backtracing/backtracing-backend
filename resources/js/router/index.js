import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

const routes = [
    {
        path: '/',
        component: () => import('@layouts/MainLayout.vue'),
        redirect: 'dashboard',
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: () => import('@pages/Dashboard.vue'),
                meta: { title: 'global.dashboard' }
            },
            {
                path: 'projects',
                name: 'projects',
                component: () => import('@pages/Projects.vue'),
                meta: { title: 'global.projects' }
            },
            {
                path: 'logs',
                name: 'logs',
                component: () => import('@pages/Logs.vue'),
                meta: { title: 'global.logs' }
            },
            {
                path: 'logs/:idLog/entries',
                props: true,
                name: 'entries',
                component: () => import('@pages/Entries.vue'),
                meta: { title: 'global.entries' }
            },
            {
                path: 'ajustes',
                name: 'ajustes',
                component: () => import('@pages/Ajustes.vue'),
                meta: { title: 'global.ajustes' }
            },
        ],
    }
];

let routerInstance = new Router({
    mode: 'history',
    base: '/admin',
    routes
});

export default routerInstance;