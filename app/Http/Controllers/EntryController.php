<?php

namespace App\Http\Controllers;

use App\Http\Requests\EntryPostRequest;
use App\Models\Entry;
use App\Models\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log as LaravelLog;

class EntryController extends Controller
{
    public function getEntriesByIdLog( $id )
    {
        $result = Entry::
            select('projects.project_name as project_name', 'logs.uri as uri', 'entries.*')    
            ->join('logs', 'logs.id', '=', 'entries.log_id')
            ->join('projects', 'projects.id', '=', 'logs.project_id')
            ->where('entries.log_id', '=', $id)
            ->orderBy('entries.id', 'DESC')
            ->get();
    
        return  response()->json($result);
    }

    public function getNewEntries( $log_id, $entry_id )
    {
        $entries = Entry::where('log_id', $log_id)->where('id','>', $entry_id)->orderBy('entries.id', 'DESC')->get();
        return  response()->json($entries);
    }

    public function clearLog( $id )
    {
        Entry::where('log_id', '=', $id)->delete();
        return  response()->json();
    }

    public function storeEntry($project_id, EntryPostRequest $request) 
    {
        $entriesData = $request->validated();
        $metaInfo = $entriesData["__meta"];

        LaravelLog::info($request);

        $log = Log::where('uri', '=', $metaInfo['uri'])->where('project_id', '=', $project_id)->first();
        if ($log === null) {
            $log = Log::create([
                'project_id' => $project_id,
                'uri' => $metaInfo['uri'],
                'method' => $metaInfo['method']
            ]);
        }
        
        $entries = Entry::create([
            'log_id' => $log->id,
            'ip' => $metaInfo['ip'],
            'status_code' => $entriesData["request"]["status_code"],
            'datetime' => $metaInfo['datetime'],
            'request_info' => json_encode(@$entriesData["request"]),
            'models_info' => json_encode(@$entriesData["models"]),
            'exceptions_info' => json_encode(@$entriesData["exceptions"]),
            'duration' => $entriesData["request"]['duration'],
        ]); 

        response()->json($entries);
    }
}
