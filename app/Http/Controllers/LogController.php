<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Log;
use Illuminate\Http\Request;

class LogController extends Controller
{

    public function getProjectsWithLogs(Request $request)
    {
        $sort_by = $request->get('sort_by', 'id');
        $sort_desc = filter_var($request->get('sort_desc', false), FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC';
        $per_page = $request->get('per_page', 10) ;
        
        $projects = Project::has('logs')
                    ->with('logs')
                    ->orderBy( 'projects.'.$sort_by , $sort_desc )
                    ->paginate( $per_page );
        return response()->json($projects);
    }

    public function getNewLogs( Request $request )
    {
        $logs = [];
        $projectsWithIdLogs = $request->projectsWithIdLogs;
        foreach ($projectsWithIdLogs as $project){
            $project = json_decode($project, true);
            $logs[$project["project_id"]] = Log::where('project_id', $project["project_id"])->where('id','>', $project["log_id"])->get();
        }
        return  response()->json($logs);
    }


}
