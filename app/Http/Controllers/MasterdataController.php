<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MasterdataController extends Controller
{
    public function getMasterdata()
    {
        $masterdata = [
            'title' => '',
            'change_status' => '',
        ];
        
        return response()->json($masterdata);
    }
}
