<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectPostRequest;
use App\Models\Project;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_by = $request->get('sort_by', 'id');
        $sort_desc = filter_var($request->get('sort_desc', false), FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC';
        $per_page = $request->get('per_page', 10) ;
        
        $projects = Project::orderBy($sort_by , $sort_desc)
                    ->paginate( $per_page );
        return response()->json($projects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ProjectPostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectPostRequest $request)
    {
        $projectData = $request->validated();
        $project = Project::create($projectData); 
        return response()->json($project);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        
        return response()->json($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ProjectPostRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectPostRequest $request, $id)
    {
        $project = Project::find($id);
        $projectData = $request->validated();
        
        $project->update($projectData);
        
        return response()->json($project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->delete();

        return response()->json();
    }

    


    public function pingRat(Request $request)
    {
        $response = Http::get($request->project_url.'backtracing/ping');
        return $response->getBody()->getContents();
    }

    public function configRat( $project )
    {
        $config = [
            'backofice_url' => Request::url(),
            'project_id' => $project->id,
            'mode' => '',
            'enabled' => true,
            'collectors' => [ 'models', 'request'],
        ];

        
        $response = Http::post($project->url.'backtracing/config', $config);
        return $response->getBody()->getContents();
    }
}
