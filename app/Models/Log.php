<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    protected $fillable = ['project_id', 'uri', 'method'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }
}
