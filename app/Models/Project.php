<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['project_name', 'url', 'status', 'token', 'static_ip', 'mode'];

    public function logs()
    {
        return $this->hasMany(Log::class);
    }
}
