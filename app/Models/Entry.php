<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use HasFactory;

    protected $fillable = ['log_id', 'ip', 'datetime', 'status_code','request_info', 'models_info', 'duration'];

    protected $casts = [
        'request_info' => 'array',
        'models_info' => 'array'
    ];

    public function log()
    {
        return $this->belongsTo(Log::class);
    }
}
