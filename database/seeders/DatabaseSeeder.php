<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'pepe',
            'email' => 'pepe@pepe.com',
            'password' => '$2y$10$3sN1vmPUbgGdO16hzYMITucE4aQYXpnfu.vYU5BX96qZOf.phX.iu'
        ]);

        Project::create([
            'project_name' => "Localhost",
            'url' => "http://127.0.0.1:8000/",
            'status' => "Status",
            'token' => "Token",
            'static_ip' => 0,
            'mode' => "Mode"
        ]);
        // $this->call([
        //     ProjectSeeder::class,
        //     LogSeeder::class,
        //     EntrySeeder::class,
        // ]);
    }
}
