<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numProjects = 50;
        $faker = Faker::create();
        
        for($i = 0; $i < $numProjects; $i++){
            Project::create([
                'project_name' => $faker->userName,
                'description' => $faker->sentence,
                'status' => $faker->fileExtension,
                'token' => $faker->sha256,
                'static_ip' => $faker->boolean(),
                'mode' => $faker->emoji,
            ]);
        }
    }
}
