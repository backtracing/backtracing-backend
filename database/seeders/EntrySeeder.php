<?php

namespace Database\Seeders;

use App\Models\Entry;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

const FAKE_HEADER = [
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Accept-Language' => 'en-US;q=0.5,en;q=0.3', 'Cache-Control' => 'max-age=0', 'Upgrade-Insecure-Requests' => '1'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36', 'Accept-Encoding' => 'gzip, deflate, br', 'Accept-Language' => 'en-US;q=0.5,en;q=0.3', 'Cache-Control' => 'max-age=0', 'DNT' => '1', 'Upgrade-Insecure-Requests' => '1'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 'Cache-Control' => 'max-age=0', 'Referer' => 'https://google.com', 'Pragma' => 'no-cache'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Accept-Language' => 'en-US;q=0.5,en;q=0.3', 'Cache-Control' => 'max-age=0'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36', 'Accept-Encoding' => 'gzip, deflate, br', 'Accept-Language' => 'en-US;q=0.5,en;q=0.3', 'DNT' => '1'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.1805 Safari/537.36', 'Accept-Encoding' => 'gzip, deflate, br', 'Accept-Language' => 'en-US;q=0.5,en;q=0.3', 'Cache-Control' => 'max-age=0', 'Referer' => 'https://google.com'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36', 'Referer' => 'https://google.com'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 'Accept-Encoding' => 'gzip, deflate, br', 'Accept-Language' => 'en-US;q=0.5,en;q=0.3', 'Cache-Control' => 'max-age=0', 'Pragma' => 'no-cache'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'Cache-Control' => 'max-age=0', 'DNT' => '1', 'Upgrade-Insecure-Requests' => '1', 'Referer' => 'https://google.com', 'Pragma' => 'no-cache'],
    ['Accept' => '*/*', 'Connection' => 'keep-alive', 'User-Agent' => 'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.158 Safari/537.36', 'Accept-Encoding' => 'gzip, deflate, br', 'Accept-Language' => 'en-US;q=0.5,en;q=0.3', 'Cache-Control' => 'max-age=0', 'Upgrade-Insecure-Requests' => '1', 'Referer' => 'https://google.com']
];

class EntrySeeder extends Seeder
{
    

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numEntry = 300;
        $faker = Faker::create();
        
        for($i = 0; $i < $numEntry; $i++){
            $array_request = [];
            $array_response = [];
            $array_body = [];
            for($j = 0; $j < 5; $j++){
                array_push( $array_request, [$faker->word(5) => $faker->word(5), $faker->word(5) => $faker->word(5)]);
                array_push( $array_response, [$faker->word(5) => $faker->word(5)]);
                array_push( $array_body, [$faker->word(5) => $faker->word(5)]);
            }
            
            Entry::create([
                'log_id' => $faker->randomNumber(2, true),
                'ip' => $faker->localIpv4(),
                'status' => $faker->randomNumber(3, true),
                'head' => json_encode(FAKE_HEADER[$faker->randomDigit]),
                'body' => json_encode($array_body),
                'params' => json_encode($array_body),
                'request' => json_encode($array_request),
                'response' => json_encode($array_response),
                'duration' => $faker->unixTime(),
            ]);
        }
    }

    
}
