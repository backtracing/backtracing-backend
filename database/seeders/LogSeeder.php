<?php

namespace Database\Seeders;

use App\Models\Log;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numLogs = 100;
        $faker = Faker::create();
        
        for($i = 0; $i < $numLogs; $i++){
            Log::create([
                'project_id' => $faker->randomNumber(1, true),
                'uri' => "/".$faker->domainWord,
                'uri_type' => $faker->randomElement(['GET', 'POST', 'UPDATE', 'DELETE']),
            ]);
        }
    }
}
