<?php

use App\Http\Controllers\ProjectController;
use App\Http\Controllers\EntryController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\MasterdataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('masterdata', [MasterdataController::class, 'getMasterdata']);

Route::get('logs', [LogController::class, 'getProjectsWithLogs']);
Route::get('logs/new', [LogController::class, 'getNewLogs']);

Route::resource('project', ProjectController::class)->except([
    'create', 'edit'
]);
Route::get('pingrat', [ProjectController::class, 'pingRat']);

Route::get('entry/{id}', [EntryController::class, 'getEntriesByIdLog']);
Route::post('entry/{project_id}', [EntryController::class, 'storeEntry']);
Route::get('entry/{log_id}/new/{entry_id}', [EntryController::class, 'getNewEntries']);


